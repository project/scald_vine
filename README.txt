CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
Scald: Vine is a video provider allowing Scald module users
to add Vine Atoms of type video.

Scald is an innnovative take on how to handle Media Atoms in Drupal.

REQUIREMENTS
------------
This module requires the following modules:
* Scald (https://drupal.org/project/sclad)

INSTALLATION
------------
* Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------
* No configuration ;)

MAINTAINERS
-----------
Current maintainers:
* Fabien Mercier (NerOcrO) - https://drupal.org/user/1728164
